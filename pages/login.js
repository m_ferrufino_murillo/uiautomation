module.exports = {
    url: 'https://todoist.com/',
    elements: {
        loginBtn: "a[href='/users/showLogin']",
        emailTxt: "input[type=email]",
        passwordTxt: "input[type=password]"
    },
    commands: [{
        clickLoginBtn() {
            return this.clickBtn('@loginBtn')
        },
        setEmail(value) {
            return this.setValue('@emailTxt', value)
        },
        setPassword(value) {
            return this.setValue('@passwordTxt', value)
        }
    }]
};