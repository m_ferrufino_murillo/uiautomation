module.exports = {

    '@tags': ['Login'],
    'Test Login page'(browser) {
        const page = browser.page.login();
        
        page
            .navigate()
            .end();
    }
};