module.exports = {
    '@tags': ['Tasks'],
    'Test Login to Tasks'(browser) {
        browser.url('https://todoist.com/')
        .assert.visible("a[href='/users/showLogin']")
        .click("a[href='/users/showLogin']")
        .assert.visible('input[type=email]')
        .setValue('input[type=email]', 'chelo.fm@gmail.com')
        .setValue('input[type=password]', 'Tms10akt+')
        .saveScreenshot('./images/login.png')
        .click('button.submit_btn')
    },
    
    'Create a Task'(browser) {
        browser
        .click('xpath', "//a[text()='Add task']")
        .assert.elementPresent('div > .public-DraftStyleDefault-block.public-DraftStyleDefault-ltr')
        .setValue('div > .public-DraftStyleDefault-block.public-DraftStyleDefault-ltr', 'testing Nightwatch')
        .saveScreenshot('./images/createdTask.png')
        .end();
    }
};